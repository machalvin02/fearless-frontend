function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const parsedStartDate = new Date(startDate);
    const formattedStartDate = parsedStartDate.toLocaleDateString();
    console.log('Parsed Start Date:', parsedStartDate, 'Formatted Start Date:', formattedStartDate);

    const parsedEndDate = new Date(endDate);
    const formattedEndDate = parsedEndDate.toLocaleDateString();
    console.log('Parsed End Date:', parsedEndDate, 'Formatted End Date:', formattedEndDate);

    return `
      <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top" alt="${name}">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          <p class="card-text"><strong>Start Date:</strong> ${formattedStartDate}</p>
          <p class="card-text"><strong>End Date:</strong> ${formattedEndDate}</p>
        </div>
      </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
    const response = await fetch(url);

    if (!response.ok) {
        // This is where I added a new error, could be useful to look at
        throw new Error('Failed to fetch data');
        // Figure out what to do when the response is bad
    } else {
        const data = await response.json();

        const columns = [
            document.getElementById('column1'),
            document.getElementById('column2'),
            document.getElementById('column3'),
            document.getElementById('column4')
        ];

        let columnIndex = 0;

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.title;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.start;
            const endDate = details.conference.end;
            const html = createCard(title, description, pictureUrl, startDate, endDate);

            const card = document.createElement('div');
            card.classList.add('card', 'shadow', 'mb-4');
            card.innerHTML = html;


            columns[columnIndex].appendChild(card);


            columnIndex = (columnIndex + 1) % columns.length;
            } else {
                // Throw new error
                throw new Error('Failed to fetch conference details');
            }
        }
        }
    } catch (e) {
        const errorMessage = document.createElement('div');
        errorMessage.classList.add('alert', 'alert-danger', 'my-4');
        errorMessage.textContent = 'An error occurred while fetching data. Please try again later';
        document.body.insertBefore(errorMessage, document.body.firstChild);
        console.error(e);
        // Handle error and also show allert
    }
    });
