window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/states/";

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('state');
        for (let state of data.states) {

            const option = document.createElement('option');

            option.value = state.abbreviation;

            option.innerHTML = state.name;

            selectTag.appendChild(option);
        }
    }
});

// Submit
window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('create-location-form');
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
         // This will prevent the default form submission
        const formData = new FormData(form)
        const jsonData = Object.fromEntries(formData);
        const jsonString = JSON.stringify(jsonData);


        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: jsonString,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            form.reset();
            const newLocation = await response.json();
            console.log('New location:', newLocation);
        } else {
            console.error('Error submitting form data');
            }
    })
})
